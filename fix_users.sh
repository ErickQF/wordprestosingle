#/bin/bash

site_path=$1
site_id=$(cat /srv/${site_path}/www/wp-config.php | grep 'table_prefix' | awk '{ print $3 }' | awk -F'_' '{print $2}')
database_name=$(cat /srv/${site_path}/etc/php/lib/CloudezSettings.php  | grep CEZ_DBNAME | awk '{print $2}' | cut -d "'" -f2) 

echo "Site path $site_path"
echo "Site id $site_id"
echo "Site database $database_name"
echo -e "Users count: "
mysql ${database_name} -e "SELECT COUNT(*) FROM wp_${site_id}_users"
echo -e "Meta count: "
mysql ${database_name} -e "SELECT COUNT(*) FROM wp_${site_id}_usermeta"
echo "Press enter to Confirm"

#cat <<EOF > /srv/remote/db/users_${site_id}.sql 
#CREATE TABLE \`wp_${site_id}_users\` (
#  \`ID\` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
#  \`user_login\` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_pass\` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_nicename\` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_email\` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_url\` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_registered\` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
#  \`user_activation_key\` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`user_status\` int(11) NOT NULL DEFAULT '0',
#  \`display_name\` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
#  \`spam\` tinyint(2) NOT NULL DEFAULT '0',
#  \`deleted\` tinyint(2) NOT NULL DEFAULT '0',
#  PRIMARY KEY (\`ID\`),
#  KEY \`user_login_key\` (\`user_login\`),
#  KEY \`user_nicename\` (\`user_nicename\`),
#  KEY \`user_email\` (\`user_email\`)
#) ENGINE=InnoDB;
#EOF

# get user and user meta data
USR_ID_LIST=$(mysql bla2 -e "SELECT GROUP_CONCAT(id SEPARATOR ',') FROM wp_users WHERE id IN (SELECT user_id FROM wp_usermeta WHERE meta_key LIKE 'wp\_${site_id}\_capa%')" -N)
echo "USR_ID_LIST = ${USR_ID_LIST}"
#mysqldump bla2 wp_users --where="id IN ($USR_ID_LIST)" --no-create-info --skip-set-charset --skip-add-locks --skip-comments | grep -v '^\/\*![0-9]\{5\}.*\/;$' | sed -e "s/wp_users/wp_${site_id}_users/g" >> /srv/remote/db/users_${site_id}.sql

mysqldump bla2 wp_users --where="id IN ($USR_ID_LIST)" | sed -e "s/wp_users/wp_${site_id}_users/g" > /srv/remote/db/users_${site_id}.sql

echo "get user meta id list"
USRMETA_ID_LIST=$(mysql bla2 -e "SET GLOBAL  group_concat_max_len = 5555555;SELECT GROUP_CONCAT(umeta_id SEPARATOR ',') FROM wp_usermeta WHERE user_id IN (SELECT user_id FROM wp_usermeta WHERE meta_key LIKE 'wp\_${site_id}\_capa%') AND (meta_key NOT LIKE 'wp\_%\_%' OR meta_key LIKE 'wp\_${site_id}\_%')" -N)

mysqldump bla2 wp_usermeta --where="umeta_id IN ($USRMETA_ID_LIST)" --no-create-info --skip-set-charset --skip-add-locks --skip-comments | grep -v '^\/\*![0-9]\{5\}.*\/;$' | sed -e "s/wp_usermeta/wp_${site_id}_usermeta/g" >> /srv/remote/db/users_${site_id}.sql

echo "import data"
mysql ${database_name} -e "DROP TABLE wp_${site_id}_users; DELETE FROM wp_${site_id}_usermeta"
mysql ${database_name} < /srv/remote/db/users_${site_id}.sql

echo -e "Users count: "
mysql ${database_name} -e "SELECT COUNT(*) FROM wp_${site_id}_users"
echo -e "Meta count: "
mysql ${database_name} -e "SELECT COUNT(*) FROM wp_${site_id}_usermeta"
