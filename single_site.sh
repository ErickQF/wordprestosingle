#/bin/bash

echo "Start migration process"
site_path=$1
site_domain=$2
site_id=$(cd /srv/migrationteste.com/www/ && wp site list --allow-root | grep ${site_domain} | awk '{print $1}')
database_name=$(cat /srv/${site_path}/etc/php/lib/CloudezSettings.php  | grep CEZ_DBNAME | awk '{print $2}' | cut -d "'" -f2) 

echo "Domain $site_path"
echo "Site ID $site_id"
echo "Database name $database_name"
[[ -z "$site_id" ]] && exit 4

echo "Clean website database tables"
mysql migrationteste -e "DROP TABLE IF EXISTS wp_${site_id}_users; DROP TABLE IF EXISTS wp_${site_id}_usermeta" || exit 1

echo "Create table wp_user e wp_usermeta"
mysql migrationteste -e "CREATE TABLE wp_${site_id}_users LIKE wp_users; INSERT wp_${site_id}_users SELECT * FROM wp_users" || exit 2
mysql migrationteste -e "CREATE TABLE wp_${site_id}_usermeta LIKE wp_usermeta; INSERT wp_${site_id}_usermeta SELECT * FROM wp_usermeta;" || exit 3

echo "Export website tables"
website_tables=$(mysql migrationteste -e "show tables" | grep "wp_${site_id}_")
mysqldump migrationteste ${website_tables} > /srv/remote/db/wp_${site_id}.sql || exit 4

echo "Clean and import database"
mysql -e "DROP DATABASE ${database_name}; CREATE DATABASE ${database_name}"
mysql ${database_name} < /srv/remote/db/wp_${site_id}.sql || exit 5

echo "Sync media files"
rsync -avH --exclude 'sites' /srv/remote/html/ /srv/${site_path}/www/
rsync -avH /srv/remote/html/wp-content/uploads/sites/${site_id}/ /srv/${site_path}/www/wp-content/uploads/
#rm -rf /srv/${site_path}/www/wp-content/uploads/sites/
rsync -avH root@cloudbr:/home/migration/migrations/aws/imedicinapro/uploads/sites/${site_id}/ \
    /srv/${site_path}/www/wp-content/uploads/
rsync -avH root@cloudbr:/home/migration/migrations/aws/wpimedicina/wp-content/uploads/2018/06/ \
    /srv/${site_path}/www/wp-content/uploads/2018/06/
rsync -avH root@cloudbr:/home/migration/migrations/aws/imedicina-pro-base/wp-content/uploads/sites/${site_id}/ \
    /srv/${site_path}/www/wp-content/uploads/

find /srv/${site_path}/www/wp-content/uploads -type d | egrep '(2016|2017|2018|2019|2020|2021)\/[0-9]+/[0-9]+' | while read path; do base_path="$(dirname $path)"; mv ${path}/* ${base_path}/.; rm -rf ${path}; done

echo "Updating wp-config"
mv /srv/${site_path}/www/wp-config.php /srv/${site_path}/www/wp-config-multi &&
cp /srv/${site_path}/etc/php/lib/wp-config-cloudez.php /srv/${site_path}/www/wp-config.php &&
sed -i "s/table_prefix  = 'wp_';/table_prefix  = 'wp_${site_id}_';/" /srv/${site_path}/www/wp-config.php &&
cd /srv/${site_path}/www && wp plugin activate elementor elementor-pro export-media-library insert-headers-and-footers worker megamenu nginx-helper shortcodes-ultimate wordpress-importer wordpress-mu-domain-mapping wp-user-avatar wordpress-seo  autoptimize export-media-library insert-headers-and-footers --allow-root
cez --fix-perm ${site_path}

echo "Clean migration"
rm /srv/remote/db/wp_${site_id}.sql
#mysqldump migrationteste -e "DROP TABLES ${website_tables}"

/srv/remote/fix_users.sh $site_path

grep webapp /srv/${site_path}/etc/nginx/server.conf
